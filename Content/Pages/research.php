<section class="intro">
    <div class="row">
        <div class="col">
            <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa.
            </p>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
            </ul>
        </div>
    </div>
</section>