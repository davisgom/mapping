<section class="homepage-intro  text-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-10">
        <h2 class="intro-text">
          Mapping MSU's Impact<br class="d-none d-sm-block" />
          Locally, Nationally, and Globally
        </h2>

        <br />

        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur totam at quia, numquam autem quibusdam eum molestias ut aliquid itaque libero obcaecati corporis laborum eveniet molestiae minus.
        </p>

        <a href="about" class="btn btn-theme-outline btn-theme-outline-reversed">
          Learn More
        </a>
      </div>
    </div>
  </div>
</section>

<section class="data-views">
  <div class="container">
    <div class="row">
      <a href="maps" class="col-md data-view map-view">
        <i class="bi bi-geo-alt-fill"></i>
        <h2>Maps</h2>
        <p>Aspernatur totam at quia numquam autem quibusdam eum molestias ut aliquid.</p>
        <div class="btn btn-theme btn-theme-accent">View Maps</div>
      </a>


      <a href="catalog" class="col-md data-view catalog-view">
        <i class="bi bi-file-text-fill"></i>
        <h2>Catalog</h2>
        <p>Aspernatur totam at quia numquam autem quibusdam eum molestias ut aliquid.</p>
        <span class="btn btn-theme btn-theme-accent">Search Catalog</span>
      </a>
    </div>
  </div>
</section>

<hr class="mt-n2" />
<br />

<section class="container homepage-map-views">
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="h5 mt-0">Map Quick Links</h2>
    </div>
  </div>

  <div class="row">

    <a class="map-view-item col-12 col-md" href="lansing-map">
      <img src="Content/Images/lansing.jpg" />
      
      <span>
        <h2>Lansing <br class="d-none d-md-block" /> Area</h2>
        <p>
          view map
        </p>
      </span>
    </a>

    <a class="map-view-item col-12 col-md" href="detroit-map">
      <img src="Content/Images/detroit.jpg" />
      
      <span>
        <h2>Detroit <br class="d-none d-md-block" /> Area</h2>
        <p>
          view map
        </p>
      </span>
    </a>

    <a class="map-view-item col-12 col-md" href="grand-rapids-map">
      <img src="Content/Images/grand-rapids.jpg" />
      
      <span>
        <h2>Grand <br class="d-none d-md-block" /> Rapids</h2>
        <p>
          view map
        </p>
      </span>
    </a>

    <a class="map-view-item col-12 col-md" href="flint-map">
      <img src="Content/Images/flint.jpg" />
      
      <span>
        <h2>Flint <br class="d-none d-md-block" /> Area</h2>
        <p>
          view map
        </p>
      </span>
    </a>

    <a class="map-view-item col-12 col-md" href="northern-michigan-map">
      <img src="Content/Images/northern-michigan.jpg" />
      
      <span>
        <h2>Northern <br class="d-none d-md-block" /> Michigan</h2>
        <p>
          view map
        </p>
      </span>
    </a>

    <a class="map-view-item col-12 col-md" href="us-map">
      <img src="Content/Images/united-states.jpg" />
      
      <span>
        <h2>United <br class="d-none d-md-block" /> States</h2>
        <p>
          view map
        </p>
      </span>
    </a>

  </div>
</section>

<br />
<hr />
<br />
<br />

<section>
  <div class="container">
    <div class="row">
      
      <div class="col-12 col-sm-8 offset-sm-1">
        <h2>
          Submit Information
        </h2>

        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia corrupti eveniet voluptas? Nihil perspiciatis ea consequuntur optio repellat eum possimus distinctio quisquam voluptate, atque vero iste temporibus porro quo aut?
          </p>

          <a href="#" class="btn btn-theme btn-theme-75 btn-theme-primary">Learn More</a>
      </div>
    </div>
  </div>
</section>

<br />
<br />
<br />