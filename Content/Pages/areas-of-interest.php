<?php

$page_title="Areas of Interest";

?>

<section class="container">
  <div class="row">
    <div class="col-12">
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum doloribus quidem alias aliquid, aut illum atque cum ab iure impedit neque facere perspiciatis dolore nisi numquam optio, non cupiditate sequi. Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam odio suscipit repellat iure distinctio at illum perspiciatis nam ut reprehenderit ipsa pariatur libero, eum, animi aspernatur, non fuga tenetur laudantium.
      </p>
      
      <hr />
      <br />
    </div>
  </div>
</section>

<section class="catalog-search-tags">
  <div class="container">
    <div class="row">   
      <div class="col-12">
        <ul class="tags list-inline">
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-5" for="add-area-5">Add tag 'Diversity, Equity, and Inclusion' to your search to reduce the result set to 17 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-5">Diversity, Equity, and Inclusion <span class="badge badge-area">17</span></a>
              </li>

              <li class="list-inline-item"><label class="sr-only" id="label-add-area-3" for="add-area-3">Add tag 'Community and Economic Development' to your search to reduce the result set to 16 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-3">Community and Economic Development <span class="badge badge-area">16</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-8" for="add-area-8">Add tag 'Governance and Public Policy' to your search to reduce the result set to 16 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-8">Governance and Public Policy <span class="badge badge-area">16</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-9" for="add-area-9">Add tag 'Health and Health Care' to your search to reduce the result set to 16 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-9">Health and Health Care <span class="badge badge-area">16</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-2" for="add-area-2">Add tag 'Children, Youth, and Family (non-school related)' to your search to reduce the result set to 14 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-2">Children, Youth, and Family (non-school related) <span class="badge badge-area">14</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-13" for="add-area-13">Add tag 'Public Understanding and Adult Learning' to your search to reduce the result set to 11 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-13">Public Understanding and Adult Learning <span class="badge badge-area">11</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-4" for="add-area-4">Add tag 'Cultural Institutions and Programs' to your search to reduce the result set to 9 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-4">Cultural Institutions and Programs <span class="badge badge-area">9</span></a></li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-6" for="add-area-6">Add tag 'Education, Pre-Kindergarten through 12th Grade' to your search to reduce the result set to 8 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-6">Education, Pre-Kindergarten through 12th Grade <span class="badge badge-area">8</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-1" for="add-area-1">Add tag 'Business and Industrial Development' to your search to reduce the result set to 7 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-1">Business and Industrial Development <span class="badge badge-area">7</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-10" for="add-area-10">Add tag 'Labor Relations, Training, and Workplace Safety' to your search to reduce the result set to 6 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-10">Labor Relations, Training, and Workplace Safety <span class="badge badge-area">6</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-11" for="add-area-11">Add tag 'Natural Resources, Land Use, and Environment' to your search to reduce the result set to 5 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-11">Natural Resources, Land Use, and Environment <span class="badge badge-area">5</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-12" for="add-area-12">Add tag 'Public Safety, Security, and Corrections' to your search to reduce the result set to 5 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-12">Public Safety, Security, and Corrections <span class="badge badge-area">5</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-14" for="add-area-14">Add tag 'Science and Technology' to your search to reduce the result set to 2 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-14">Science and Technology <span class="badge badge-area">2</span></a>
              </li>
              
              <li class="list-inline-item"><label class="sr-only" id="label-add-area-7" for="add-area-7">Add tag 'Food and Fiber Production and Safety' to your search to reduce the result set to 1 matches</label>
                <a href="catalog-results" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-7">Food and Fiber Production and Safety <span class="badge badge-area">1</span></a>
              </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="container">
  <div class="row">
    <div class="col-12">
      <br />
      
      <a href="catalog-results">View full catalog</a>

      <br />
      <hr />
      <br />
      
      <?php include "search-form.php"; ?>

    </div>
  </div>  
</section>
