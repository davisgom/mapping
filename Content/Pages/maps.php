<?php $page_title="Maps" ?>
<p>
Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repudiandae soluta facilis hic rerum, labore vero adipisci ducimus, nulla eos inventore sunt magnam quod qui error, quis deleniti debitis expedita. Qui! 
</p>

<br />
<hr />
<br />

<section class="container map-views">
  <div class="row">

    <div class="map-view-item">
      <img src="Content/Images/lansing.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="lansing-map">Lansing Area</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="lansing-map">
          view map
        </a>
      </span>
    </div>

    <div class="map-view-item">
      <img src="Content/Images/detroit.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="detroit-map">Detroit</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="detroit-map">
          view map
        </a>
      </span>
    </div>

    <div class="map-view-item">
      <img src="Content/Images/grand-rapids.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="grand-rapids-map">Grand Rapids</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="grand-rapids-map">
          view map
        </a>
      </span>
    </div>

    <div class="map-view-item">
      <img src="Content/Images/flint.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="flint-map">Flint</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="flint-map">
          view map
        </a>
      </span>
    </div>

    <div class="map-view-item">
      <img src="Content/Images/northern-michigan.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="northern-michigan-map">Northern Michigan</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="northern-michigan-map">
          view map
        </a>
      </span>
    </div>

    <div class="map-view-item">
      <img src="Content/Images/united-states.jpg" alt="" class="img-fluid">
      
      <span>
        <h2><a href="us-map">United States</a></h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

        <a class="btn btn-theme btn-theme-65 btn-theme-primary" href="us-map">
          view map
        </a>
      </span>
    </div>

  </div>
</section>