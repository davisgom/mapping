<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
  $("input").focus(function(){
    var element = document.getElementById("search-filter");
    element.classList.add ("show");
  });
});
</script>

<?php include "search-form.php"; ?>

<section class="search-filter container collapse" id="search-filter">
  
  <div class="row">
    <div class="col-12 d-flex">
        <ul class="tags list-inline">
            <li class="list-inline-item"><span class="btn btn-tag btn-tag-area btn-std">Cultural Institutions and Programs <label id="label-area-4" class="sr-only" for="area-4">Remove 'Cultural Institutions and Programs' from your search to see more results.</label><a id="area-4" href="#" title="Remove this area to see more">&nbsp;<i class="bi bi-x-circle-fill"></i></a></span></li>

            <li class="list-inline-item"><span class="btn btn-tag btn-tag-college btn-std">Libraries <label class="sr-only" for="college-26">Remove 'Libraries' from your search to see more results.</label><a id="college-26" href="#" title="Remove this college to see more">&nbsp;<i class="bi bi-x-circle-fill"></i></a></span></li>
        </ul>
    </div>
  </div>  




  <div class="row">
    <div class="col-10">

      <h2 class="mt-4">Focus your search</h2>
      <p>
        Select one or more tags to limit your search results to an area of interest or a college. The number indicates how many results in your current search match the tag.
      </p>

      <hr class="my-4" />

      <h3>Area of Interest</h3>

      <ul class="tags list-inline">
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-5" for="add-area-5">Add tag 'Diversity, Equity, and Inclusion' to your search to reduce the result set to 17 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-5">Diversity, Equity, and Inclusion <span class="badge badge-area">17</span></a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-3" for="add-area-3">Add tag 'Community and Economic Development' to your search to reduce the result set to 16 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-3">Community and Economic Development <span class="badge badge-area">16</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-8" for="add-area-8">Add tag 'Governance and Public Policy' to your search to reduce the result set to 16 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-8">Governance and Public Policy <span class="badge badge-area">16</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-9" for="add-area-9">Add tag 'Health and Health Care' to your search to reduce the result set to 16 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-9">Health and Health Care <span class="badge badge-area">16</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-2" for="add-area-2">Add tag 'Children, Youth, and Family (non-school related)' to your search to reduce the result set to 14 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-2">Children, Youth, and Family (non-school related) <span class="badge badge-area">14</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-13" for="add-area-13">Add tag 'Public Understanding and Adult Learning' to your search to reduce the result set to 11 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-13">Public Understanding and Adult Learning <span class="badge badge-area">11</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-4" for="add-area-4">Add tag 'Cultural Institutions and Programs' to your search to reduce the result set to 9 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-4">Cultural Institutions and Programs <span class="badge badge-area">9</span></a></li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-6" for="add-area-6">Add tag 'Education, Pre-Kindergarten through 12th Grade' to your search to reduce the result set to 8 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-6">Education, Pre-Kindergarten through 12th Grade <span class="badge badge-area">8</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-1" for="add-area-1">Add tag 'Business and Industrial Development' to your search to reduce the result set to 7 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-1">Business and Industrial Development <span class="badge badge-area">7</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-10" for="add-area-10">Add tag 'Labor Relations, Training, and Workplace Safety' to your search to reduce the result set to 6 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-10">Labor Relations, Training, and Workplace Safety <span class="badge badge-area">6</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-11" for="add-area-11">Add tag 'Natural Resources, Land Use, and Environment' to your search to reduce the result set to 5 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-11">Natural Resources, Land Use, and Environment <span class="badge badge-area">5</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-12" for="add-area-12">Add tag 'Public Safety, Security, and Corrections' to your search to reduce the result set to 5 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-12">Public Safety, Security, and Corrections <span class="badge badge-area">5</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-14" for="add-area-14">Add tag 'Science and Technology' to your search to reduce the result set to 2 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-14">Science and Technology <span class="badge badge-area">2</span></a>
        </li>
        
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-7" for="add-area-7">Add tag 'Food and Fiber Production and Safety' to your search to reduce the result set to 1 matches</label>
          <a href="#" class="btn btn-tag btn-outline-tag-area" type="button" role="button" id="add-area-7">Food and Fiber Production and Safety <span class="badge badge-area">1</span></a>
        </li>
  </ul>

  
  <hr class="my-4" />

  <h3>Colleges and Departments</h3>

  <ul class="tags list-inline">
    <li class="list-inline-item"><label class="sr-only" id="label-add-area-19" for="add-area-19">Add tag 'College of Social Science' to your search to reduce the result set to 8 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-19">College of Social Science <span class="badge badge-college">8</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-11" for="add-area-11">Add tag 'College of Education' to your search to reduce the result set to 6 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-11">College of Education <span class="badge badge-college">6</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-13" for="add-area-13">Add tag 'College of Human Medicine' to your search to reduce the result set to 4 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-13">College of Human Medicine <span class="badge badge-college">4</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-8" for="add-area-8">Add tag 'College of Agriculture and Natural Resources' to your search to reduce the result set to 4 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-8">College of Agriculture and Natural Resources <span class="badge badge-college">4</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-31" for="add-area-31">Add tag 'Provost and Academic Affairs' to your search to reduce the result set to 4 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-31">Provost and Academic Affairs <span class="badge badge-college">4</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-4" for="add-area-4">Add tag 'Vice Provost for University Outreach and Engagement' to your search to reduce the result set to 3 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-4">Vice Provost for University Outreach and Engagement <span class="badge badge-college">3</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-18" for="add-area-18">Add tag 'College of Osteopathic Medicine' to your search to reduce the result set to 2 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-18">College of Osteopathic Medicine <span class="badge badge-college">2</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-3" for="add-area-3">Add tag 'Vice Provost for University Arts and Collections' to your search to reduce the result set to 1 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-3">Vice Provost for University Arts and Collections <span class="badge badge-college">1</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-16" for="add-area-16">Add tag 'College of Natural Science' to your search to reduce the result set to 1 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-16">College of Natural Science <span class="badge badge-college">1</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-10" for="add-area-10">Add tag 'College of Communication Arts and Sciences' to your search to reduce the result set to 1 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-10">College of Communication Arts and Sciences <span class="badge badge-college">1</span>
      </a>
    </li>

    <li class="list-inline-item"><label class="sr-only" id="label-add-area-26" for="add-area-26">Add tag 'Libraries' to your search to reduce the result set to 1 matches</label>
      <a href="#" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-26">Libraries <span class="badge badge-college">1</span>
      </a>
    </li>
  </ul>

  <hr class="my-4" />

  <a data-toggle="collapse" href="#search-filter" >
    <i class="bi bi-x-circle"></i> Close
  </a>

    </div>
  </div>
</section>

<section class="project-data container">

<p class="border-0">
<?php

if ($page_content == "lansing-map-data") {echo 'Searching <strong>Lansing</strong> produced <strong>101 results.</strong>';}

elseif ($page_content == "detroit-map-data") {echo 'Searching <strong>Detroit</strong> produced <strong>161 results.</strong>';}

elseif ($page_content == "grand-rapids-map-data") {echo 'Searching <strong>Grand Rapids</strong> produced <strong>29 results.</strong>';}

elseif ($page_content == "flint-map-data") {echo 'Searching <strong>Flint</strong> produced <strong>33 results.</strong>';}

elseif ($page_content == "northern-michigan-map-data") {echo 'Searching <strong>Northern Michigan</strong> produced <strong>249 results.</strong>';}

elseif ($page_content == "us-map-data") {echo 'Searching <strong>United States</strong> produced <strong>35 results.</strong>';}

else {echo "Results 1 to 25 of <strong>560 results</strong>:";}

?>  


</p>

<br />

<?php
$pname = array("Lorem ipsum dolor", "Ullam fuga dolore nam architecto", "Consequatur alias aperiam");
$fname = array("Will", "Willis", "Willa", "William", "Willemina");

$city = array(
    "Detroit",
    "Lansing",
    "Grand Rapids",
    "Ann Arbor",
    "Traverse City",
    "Flint",
    "Saginaw",
    "Kalamazoo",
    "Muskegon",
    "Farmington Hills",
    "Frankenmuth",
    "Dearborn",
    "Warren",
    "Battle Creek",
    "Livonia",
    "Petosky",
    "Sparta", 
);

for ($i = 1;$i <= 25; $i++) {
    echo '
    <div class="row">
        <div class="col-12">
            <h2><a href="project-report">'.$pname[array_rand($pname)].'</a></h2>
            <ul>
                <li>
                    <i class="bi bi-person-fill"></i>
                    <span class="sr-only">Project Contact:</span> '.$fname[array_rand($fname)].' Spartan
                </li>
                               
                <li>
                    <i class="bi bi-bank2"></i>
                    <span class="sr-only">College/MAU:</span> College of Lorem Ipsum
                </li>

                <li>
                    <i class="bi bi-geo-alt-fill"></i>
                    <span class="sr-only">Location:</span> '.$city[array_rand($city)].'
                </li>
            </ul>

            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ullam fuga dolore nam architecto ad odit. Consequatur alias aperiam deleniti! Maiores aperiam mollitia dolores! Libero incidunt numquam, quo in fugit ducimus.
            </p>

            <a href="project-report" class="btn btn-theme btn-theme-primary">View Project Report</a>
        </div>
    </div>
    ';
    }
?>

</section>



<nav aria-label="Page navigation">
  <ul class="pagination justify-content-center">
   
  <li class="page-item disabled">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
   
    <li class="page-item disabled">
      <a class="page-link">Previous</a>
    </li>
   
    <li class="page-item active"><a class="page-link" style="margin-bottom: 5px;" href="#">1</a></li>
   
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>

    <li class="page-item"><a class="page-link" href="#">&mldr;</a></li>
    
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
    
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>