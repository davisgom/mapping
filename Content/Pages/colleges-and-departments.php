<?php

$page_title="Colleges and Deparments";

?>

<section class="container">
  <div class="row">
    <div class="col-12">
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum doloribus quidem alias aliquid, aut illum atque cum ab iure impedit neque facere perspiciatis dolore nisi numquam optio, non cupiditate sequi. Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam odio suscipit repellat iure distinctio at illum perspiciatis nam ut reprehenderit ipsa pariatur libero, eum, animi aspernatur, non fuga tenetur laudantium.
      </p>
      
      <hr />
      <br />
    </div>
  </div>
</section>

<section class="catalog-search-tags">
  <div class="container">
    <div class="row">
     <div class="col-12">
      <ul class="tags list-inline">
        <li class="list-inline-item"><label class="sr-only" id="label-add-area-19" for="add-area-19">Add tag 'College of Social Science' to your search to reduce the result set to 8 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-19">College of Social Science <span class="badge badge-college">8</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-11" for="add-area-11">Add tag 'College of Education' to your search to reduce the result set to 6 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-11">College of Education <span class="badge badge-college">6</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-13" for="add-area-13">Add tag 'College of Human Medicine' to your search to reduce the result set to 4 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-13">College of Human Medicine <span class="badge badge-college">4</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-8" for="add-area-8">Add tag 'College of Agriculture and Natural Resources' to your search to reduce the result set to 4 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-8">College of Agriculture and Natural Resources <span class="badge badge-college">4</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-31" for="add-area-31">Add tag 'Provost and Academic Affairs' to your search to reduce the result set to 4 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-31">Provost and Academic Affairs <span class="badge badge-college">4</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-4" for="add-area-4">Add tag 'Vice Provost for University Outreach and Engagement' to your search to reduce the result set to 3 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-4">Vice Provost for University Outreach and Engagement <span class="badge badge-college">3</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-18" for="add-area-18">Add tag 'College of Osteopathic Medicine' to your search to reduce the result set to 2 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-18">College of Osteopathic Medicine <span class="badge badge-college">2</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-3" for="add-area-3">Add tag 'Vice Provost for University Arts and Collections' to your search to reduce the result set to 1 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-3">Vice Provost for University Arts and Collections <span class="badge badge-college">1</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-16" for="add-area-16">Add tag 'College of Natural Science' to your search to reduce the result set to 1 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-16">College of Natural Science <span class="badge badge-college">1</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-10" for="add-area-10">Add tag 'College of Communication Arts and Sciences' to your search to reduce the result set to 1 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-10">College of Communication Arts and Sciences <span class="badge badge-college">1</span>
          </a>
        </li>

        <li class="list-inline-item"><label class="sr-only" id="label-add-area-26" for="add-area-26">Add tag 'Libraries' to your search to reduce the result set to 1 matches</label>
          <a href="catalog-results" class="btn btn-tag btn-outline-tag-college" type="button" role="button" id="add-area-26">Libraries <span class="badge badge-college">1</span>
          </a>
        </li>
      </ul>
     </div>
    </div>
  </div>
</section>

<section class="container">
  <div class="row">
    <div class="col-12">
      <br />
      
      <a href="catalog-results">View full catalog</a>

      <br />
      <hr />
      <br />
      
      <?php include "search-form.php"; ?>

    </div>
  </div>  
</section>
