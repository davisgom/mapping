<h1>Heading Level 1</h1>

<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. <q>Cras finibus interdum sodales.</q> Nunc vel lacinia ex. Duis molestie, neque vitae cursus convallis, <ins>nulla elit pretium elit,</ins> commodo pharetra justo eros eu ligula. <sub>Class aptent taciti</sub> sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla neque libero, feugiat in semper vitae, porta ut neque. <var>Vivamus nisl risus</var>, rutrum ac augue non, scelerisque pulvinar mi. <mark>Lorem ipsum dolor</mark> sit amet, consectetur adipiscing elit. <strong>Cras finibus interdum sodales.</strong> Nunc vel lacinia ex. Duis molestie, neque vitae cursus convallis, nulla elit pretium elit, commodo pharetra justo eros eu ligula. <samp>Class aptent taciti sociosqu ad litora torquent per conubia nostra,</samp> per inceptos himenaeos. Nulla neque libero, feugiat in semper vitae, porta ut neque. <cite>Vivamus nisl risus, rutrum ac augue non, scelerisque pulvinar mi.</cite> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras finibus <abbr title="Interdum">interdum</abbr> sodales. <sup>Nunc vel lacinia ex.</sup> Duis molestie, neque vitae cursus convallis, nulla elit pretium elit, commodo pharetra justo eros eu ligula. <kbd>Class aptent taciti sociosqu</kbd> ad litora torquent per conubia nostra, per inceptos himenaeos. <em>Nulla neque libero</em>, feugiat in semper vitae, porta ut neque. Vivamus nisl risus, rutrum ac augue non, scelerisque pulvinar mi. <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small> Cras finibus interdum sodales. <a href="#">Nunc vel lacinia ex.</a> Duis molestie, neque vitae cursus convallis, <x-code>nulla elit pretium elit</x-code>, commodo pharetra justo eros eu ligula. <del>Class aptent taciti sociosqu ad litora torquent per conubia nostra,</del> per inceptos himenaeos. Nulla neque libero, feugiat in semper vitae, porta ut neque. <time>Vivamus nisl risus,</time> rutrum ac augue non, scelerisque pulvinar mi. 
</p>

<br />
<hr />
<br />

<h2>Heading Level 2</h2>

<p>
Mapping is the process of creating maps, which are graphical representations of geographic areas or spaces. Mapping projects involve the use of various tools and techniques to create accurate and informative maps that can be used for a variety of purposes. These projects can range from simple tasks such as creating a map of a local area to complex tasks like creating a map of the entire world.
</p>

<br />
<hr />
<br />

<h3>Heading Level 3</h3>
<p>
There are many different types of mapping projects, each with its own set of challenges and requirements. Some mapping projects focus on creating detailed and accurate maps of specific areas, such as cities or national parks. Other mapping projects are focused on creating maps that show the distribution of particular features, such as land use or population density.
</p>

<p>
At its core, mapping is about creating visual representations of the world around us. By doing so, we can gain a deeper understanding of the complex systems that make up our planet. Whether you're a professional cartographer or simply someone who enjoys exploring the world through maps, mapping projects are a fascinating and rewarding way to learn more about the world around us.
</p>

<ul>
  <li><a href="lansing-map">Lansing Projects</a></li>
  <li><a href="detroit-map">Detroit Projects</a></li>
  <li><a href="us-map">US Projects</a></li>
</ul>

