<?php

$page_title="Catalog Search";

include "search-form.php";

?>


<section class="catalog-views">
  <div class="container">
    <div class="row">
     
      <a class="catalog-view-item col-12 col-md" href="areas-of-interest">      
        <span>
          <h2>Areas of Interest</h2>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          </p>
        </span>
      </a>

      <a class="catalog-view-item col-12 col-md" href="colleges-and-departments">      
        <span>
          <h2>Colleges and Departments</h2>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          </p>
        </span>
      </a>

      <a class="catalog-view-item col-12 col-md" href="#">      
        <span>
          <h2>Location</h2>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          </p>
        </span>
      </a>
    
    </div>
  </div>
</section>

<section class="container">
  <div class="row">
    <div class="col-12">
      <a href="catalog-results">View full catalog</a>

      <br />
      <br />
      <hr />

      <p>
        Catalog information: Michigan State University has annually collected data about community-engaged scholarship and university outreach from its faculty and academic staff since 2004. Researchers with MSU's Office of University Outreach and Engagement (UOE) developed the Outreach and Engagement Measurement Instrument (OEMI), and variants of it, in order to gather this important data.
      </p>

    </div>
  </div>  
</section>
