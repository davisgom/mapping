<?php
    $page_title="Michigan State University College Advising Corps (MSUCAC)";
?>

<br />
<section class="container" style="page-break-after: always;">
  <div class="row">
    <div class="col d-lg-flex">
      <div class="mr-lg-4 mb-4 col-lg-8" style="margin-left: -15px;">
        <p>
        MSUCAC, in close partnership with the national College Advising Corps (CAC), and the Michigan College Access Network (MCAN), is seeking to help more Michigan students pursue post-secondary education. Recent MSU graduates will serve as AmeriCorps members in selected public high schools across Michigan, particularly those that are located in communities with low socio-economic status and low adult educational attainment rates. In conjunction with school professionals, MSUCAC AmeriCorps members will serve as college advisers and will support students as they make the transition from high school to a post-secondary institution.
        </p>
        <p>
          MSU College Advising Corps (MSUCAC) is a program filled with opportunities for recent college graduates. MSUCAC places recent MSU graduates in high schools throughout the state of Michigan to serve alongside counselors and other college access organizations. These near-peer College Advisers assist high school students and their families with college searches, essay writing, SAT/ACT prep, college applications, FAFSA completion, scholarship searches, college visits, and successfully transitioning to post-secondary education, including four-year or two-year institutions and trade or technical schools.
        </p>
      </div>

      <aside>
        <div class="alert alert-info project-meta">
          <div class="results-map embed-responsive embed-responsive-1by1" style="border-radius: 8px;">
            <iframe src="https://www.arcgis.com/apps/Embed/index.html?webmap=432a8d7ca22d4b5b859e0bdaa30ae118%20%20%20%20%20%20%20%20%20&search=true&searchextent=true%20%20%20%20%20%20%20%20%20&find=Detroit%20Cristo%20Rey%20High%20School,%205679%20W%20Vernor%20Hwy,%20Detroit,%20MI,%2048209,%20USA"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>

          <br />

          <div>
            <div class="mb-2 lead font-weight-bold">
              <i class="bi bi-geo-alt-fill"></i>
              <span class="sr-only">Location:</span> Detroit
            </div>

            <div>
              Detroit Cristo Rey High School <br />
              5679 Vernor Hwy<br />
              Detroit, MI 48209
            </div>
          </div>
        </div>
      </aside>
    </div>

  </div>
</section>

<br />
<hr class="d-print-none" />
<br />

<h2 class="h5 mb-3">Project Contact:</h2>

<div class="card contact-card col-md-8">
  <div class="card-body">
    <ul class="list-unstyled">
      <li class="list-unstyled-item name">Laura Wise</li>
      <li class="list-unstyled-item title">MSUCAC Program Director; Student Services Coordinator</li>
      <li class="list-unstyled-item">Career Services and Placement</li>
      <li class="list-unstyled-item">VP Student Affairs and Services</li>

      <li class="list-unstyled-item email"><a href="mailto:lauraw@msu.edu" class="email" data-toggle="tooltip" data-html="true" title="" target="_blank" data-original-title="E-mail">lauraw@msu.edu</a></li>
    </ul>
  </div>
</div>

<br />
<hr />
<br />

<p class="mb-0 d-print-none">
  Click the tags below to find more projects in an area of interest, or by college or department.
</p>

<ul class="tags list-inline">
    <li class="list-inline-item">
        <label class="sr-only" id="label-tag-area-5" for="tag-area-5">The catalog contains 206 projects related to 'Diversity, Equity, and Inclusion'. Click to view them.</label>
        <a id="tag-area-5" class="btn btn-tag btn-tag-area" type="button" href="#">Diversity, Equity, and Inclusion <span class="badge badge-outline-area d-print-none">206</span></a>
    </li>
    <li class="list-inline-item">
        <label class="sr-only" id="label-tag-area-6" for="tag-area-6">The catalog contains 172 projects related to 'Education, Pre-Kindergarten through 12th Grade'. Click to view them.</label>
        <a id="tag-area-6" class="btn btn-tag btn-tag-area" type="button" href="#">Education, Pre-Kindergarten through 12th Grade <span class="badge badge-outline-area d-print-none">172</span></a>
    </li>
    <li class="list-inline-item">
        <label class="sr-only" id="label-tag-college-36" for="tag-college-36">The catalog contains 2 projects by faculty in VP Student Affairs and Services. Click to view them.</label>
        <a id="tag-college-36" class="btn btn-tag btn-tag-college" type="button" href="#">VP Student Affairs and Services <span class="badge badge-outline-college d-print-none">2</span></a>
    </li>
</ul>

<br />
<hr />
<br />

<h3>External Partners</h3>
<ul>
  <li>Michigan College Access Network</li>
  <li>National College Advising Corps</li>
  <li>
    Low SES schools, including

    <ul>
      <li>in Detroit: Detroit Cristo Rey School</li>
      <li>in Macomb: Warren Woods Tower High School</li>
    </ul>

  </li>
</ul>