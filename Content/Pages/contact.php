<p>
For questions and information, please contact us at:
</p>

<ul class="list-unstyled">
	<li class="h4">
		<strong>University Outreach and Engagement</strong>
  </li>
	
  <li>
		Michigan State University
  </li>
	
  <li>
		219 S. Harrison Road, Room 93
  </li>
	
  <li>
		East Lansing, Michigan 48824-1022
  </li>
	
  <li>
		Telephone: (517) 353-8977
  </li>
	
  <li>
		Fax: (517) 432-9541
  </li>
	
  <li>
		E-mail: <a href="mailto:engage@msu.edu" class="email" target="_blank">engage@msu.edu</a>
  </li>
</ul>


<br />
<hr />
<br />

<section>
  <div class="container">
    <div class="row">
      
      <div class="col-12 col-sm-9 p-0">
        <h2>
          Submit Information
        </h2>

        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis tenetur dolores ratione reprehenderit dolor ipsam mollitia, eveniet atque voluptas possimus expedita, voluptate iusto nemo suscipit dicta aliquam repellat nostrum illo!
          </p>

          <a href="#" class="btn btn-theme btn-theme-75 btn-theme-primary">Learn More</a>
      </div>
    </div>
  </div>
</section>