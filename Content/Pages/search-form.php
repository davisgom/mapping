<section class="project-search alert alert-warning py-4">
    <div class="row">      
        <div class="col-12 d-md-flex">
            <input type="text" class="form-control" placeholder="Search by keyword, topic, partner, or location" aria-label="Search Projects">

            <button class="btn btn-theme-primary" onClick="parent.location='catalog-results'" type="button">Search</button>
        </div>
    </div>
</section>