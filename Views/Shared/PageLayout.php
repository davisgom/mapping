<?php ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean(); ?>

<?php if (isset($page_title)){ $set_page_title = $page_title;}?>



<header class="page-header <?php echo $page_content . '-header'; ?>">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-sm-9 d-flex">
        <h1>
          <span class="page-title">
            <?php if (isset($page_title)){ echo $page_title; } else echo $page_content; ?>
          </span>
        </h1>
      </div>

      <div class="col-sm">
        <?php
          if (str_contains($page_content, 'data')) { echo '<a href="#search-filter" data-toggle="collapse" class="btn btn-theme-primary"><i class="bi bi-funnel-fill mr-2"></i> Filter Results</a>';}
          if (str_contains($page_content, 'results')) { echo '<a href="#search-filter" data-toggle="collapse" class="btn btn-theme-primary"><i class="bi bi-funnel-fill mr-2"></i> Filter Results</a>';}
        ?>  
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-sm-8 d-flex">
        <div class="map-desc">
          <p>
            <?php if (isset($map_desc)){ echo $map_desc; }; ?>
          </p>
          
          <p>
            <?php if (isset($map_desc)){ echo '
              <a href="#'.$page_content.'">Explore Map Below</a>
              <span class="d-none d-sm-inline">&nbsp;|&nbsp;</span> <span class="d-inline d-sm-none"><br /></span>
              <a href="'.$page_content.'-data" class="text-capitalize">Explore '.$page_title.' Data </a>';
              };
            ?>
          </p>
        </div>
      </div>
  </div>
</header>

<?php
  if (isset($map_link)){
    echo '<section class="map" id="'.$page_content.'">';
    echo '<div class="embed-responsive embed-responsive-16by9" style="min-height: 400px;">';
    echo '<iframe src="'.$map_link.'"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';
    echo '</div>';
    echo '</section>';
  }
?>

<section class="container">
  <div class="row">
    <div class="col-11">
      <?php echo $content ?>
    </div>
  </div>
</section>
