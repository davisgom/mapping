<section class="container" id="MSUheader">
	<div class="row">
		<div class="col-md-12">
			<div id="MSUmasthead" role="banner">
				<a href="http://www.msu.edu">
					<!-- if using a dark background change the img src to images/masthead-helmet-white.png-->
					<img class="screen-msugraphic" alt="Michigan State University" src="Content/Images/masthead-helmet-white.svg"/>
					<!-- MSU Web Standards indicate the MSU masthead graphic should be printed black -->
					<img class="print-msugraphic" alt="Michigan State University" src="Content/Images/masthead-helmet-green.svg"/>
				</a>
			</div>

			
			<div class="d-none d-lg-block">
				<?php //include("msu-search.php"); ?>
			</div>
		</div>
	</div>
</section>
