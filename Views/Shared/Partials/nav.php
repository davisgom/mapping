<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="sr-only">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
            <li>
              <a href="home">
                home
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "about") {echo 'class="active"';} ?> href="about">
                about
              </a>
            </li>

            <li>
            <a class="nav-link dropdown-toggle <?php if ($page_content == "projects") { echo "active";}?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Maps
            </a>

            <div class="dropdown-menu">
              <span>
                <a <?php if ($page_content == "lansing-map") {echo 'class="active"';} ?> href="lansing-map">
                  lansing area
                </a>  
                <a <?php if ($page_content == "detroit-map") {echo 'class="active"';} ?> href="detroit-map">
                  detriot
                </a>
                <a <?php if ($page_content == "grand-rapids-map") {echo 'class="active"';} ?> href="grand-rapids-map">
                  grand rapids
                </a>
                <a <?php if ($page_content == "flint-map") {echo 'class="active"';} ?> href="flint-map">
                  flint
                </a>
                <a <?php if ($page_content == "northern-michigan-map") {echo 'class="active"';} ?> href="northern-michigan-map">
                  northern michigan
                </a>
                <a <?php if ($page_content == "us-map") {echo 'class="active"';} ?> href="us-map">
                  united states
                </a>
                <div class="dropdown-divider"></div>
                <a <?php if ($page_content == "maps") {echo 'class="active"';} ?> href="maps">
                  map views
                </a>  
              </span>
            </div>
          </li>

          <li>
            <a class="nav-link dropdown-toggle <?php if ($page_content == "projects") { echo "active";}?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Catalog
            </a>

            <div class="dropdown-menu">
              <span>
                <a <?php if ($page_content == "catalog") {echo 'class="active"';} ?> href="catalog">
                  keyword search
                </a>  
                <a <?php if ($page_content == "areas-of-interest") {echo 'class="active"';} ?> href="areas-of-interest">
                  areas of interest
                </a>
                <a <?php if ($page_content == "colleges-and-departments") {echo 'class="active"';} ?> href="colleges-and-departments">
                  colleges and departments
                </a>
                <div class="dropdown-divider"></div>
                <a <?php if ($page_content == "catalog-results") {echo 'class="active"';} ?> href="catalog-results">
                  view full catalog
                </a>
              </span>
            </div>
          </li>

            <li>
              <a <?php if ($page_content == "contact") {echo 'class="active"';} ?> href="contact">
                contact
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
