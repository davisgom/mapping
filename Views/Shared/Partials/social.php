<section class="cei-social">
  <div class="container">
    <div class="row">
      <div class="connect-intro col-md-8 mx-auto my-4">
        <h2>Connect with CEI</h2>
    
        <p>
          We would love the opportunity to connect with you on social media, and share more content with you about innovation, community, and more.
        </p>
      </div>
    
      <div class="social-icons mb-5">
          <a href="#" class="btn btn-theme-social facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social youtube"><i class="fab fa-youtube" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social linkedin"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social newsletter"><i class="fa fa-envelope" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</section>